# PROCÉDURE DE MISE EN LIGNE 


## Prérequis : Composer + Symfony + Mysql installé


### Ouvrir un terminal :

Ctrl+Alt+T



### Cloner le repository :

Écrivez la commande : ```git clone https://gitlab.com/VAZFlorian/acf.git```



### Configuration de la base de données

Ouvrir le fichier .env

Commenter la ligne 31
En commentaire : ```#DATABASE_URL="postgresql://symfony:ChangeMe@127.0.0.1:5432/app?serverVersion=13&charset=utf8"```

Décommenter la ligne 30 et remplacer user, password, database par les valeurs correspondante à votre configuration mysql
 ```DATABASE_URL="mysql://user:password@127.0.0.1:3306/databasename"```



### Lancement du projet:

Ouvrez un terminal dans le dossier de votre projet et exécutez les commandes suivantes : 
- ```symfony server:start```
- ```symfony console doctrine:database:create```
- ```symfony console doctrine:migrations:migrate```


