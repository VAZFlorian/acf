<?php

namespace App\Form;

use App\Entity\Message;
use App\Entity\Prospect;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('contenu')
            ->add('dateEnvoi')
            //->add('IDProspect')


            ->add('addresseIP')
            ->add('mail')
            ->add('telephone')
            ->add('firstConnexion')
            ->add('lastConnexion')
            ->add('nom')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Message::class,
        ]);
    }
}
