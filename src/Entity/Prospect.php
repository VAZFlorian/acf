<?php

namespace App\Entity;

use App\Repository\ProspectRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProspectRepository::class)]
class Prospect
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    private $addresseIP;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    private $mail;

    #[ORM\Column(type: 'string', length: 15, nullable: true)]
    private $telephone;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $firstConnexion;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $lastConnexion;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    private $nom;

    public function getId(): ?int
    {
        return $this->id;
    }
    

    public function getAddresseIP(): ?string
    {
        return $this->addresseIP;
    }

    public function setAddresseIP(?string $addresseIP): self
    {
        $this->addresseIP = $addresseIP;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(?string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getFirstConnexion(): ?\DateTimeInterface
    {
        return $this->firstConnexion;
    }

    public function setFirstConnexion(?\DateTimeInterface $firstConnexion): self
    {
        $this->firstConnexion = $firstConnexion;

        return $this;
    }

    public function getLastConnexion(): ?\DateTimeInterface
    {
        return $this->lastConnexion;
    }

    public function setLastConnexion(?\DateTimeInterface $lastConnexion): self
    {
        $this->lastConnexion = $lastConnexion;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
}
