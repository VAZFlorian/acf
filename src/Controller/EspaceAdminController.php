<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Message;
use App\Form\MessageType;
use App\Repository\MessageRepository;

use App\Entity\Prospect;
use App\Form\ProspectType;
use App\Repository\ProspectRepository;

class EspaceAdminController extends AbstractController
{
    #[Route('/espace_admin', name: 'app_espace_admin')]
    public function index(ProspectRepository $prospectRepository, MessageRepository $messageRepository ): Response
    {
        return $this->render('espace_admin/index.html.twig', [
            'controller_name' => 'EspaceAdminController',
            'prospects' => $prospectRepository->findAll(),
            'messages' => $messageRepository->findAll(),
        ]);
    }

   
}
