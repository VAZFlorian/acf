<?php
// src/Controller/TaskController.php
namespace App\Controller;

use App\Entity\Tag;
use App\Entity\Task;
use App\Form\TaskType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route; 

use App\Entity\Prospect;
use App\Form\ProspectType;
use App\Repository\ProspectRepository;

use App\Entity\Message;
use App\Form\MessageType;
use App\Repository\MessageRepository;





/* public function myMethodAction(Request $request)
{
    $ip = $request->getClientIp();
    

} */
class TaskController extends AbstractController
{
    #[Route('/task', name: 'app_task')]
    public function new(Request $request, MessageRepository $messageRepository,ProspectRepository $prospectRepository ): Response
    {
        $task = new Task();

        // dummy code - add some example tags to the task
        // (otherwise, the template will render an empty list of tags)
        $tag1 = new Tag();
        $tag1->setName('nom');
        $task->getTags()->add($tag1);
        $tag2 = new Tag();
        $tag2->setName('mail');
        $task->getTags()->add($tag2);
        $tag3 = new Tag();
        $tag3->setName('message');
        $task->getTags()->add($tag3);
        // end dummy code

        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {

            // Chercher "mail" parmi tous les prospects
            // => Si "mail" trouvé 
            $prospect = $prospectRepository->findOneByMail(mail);
            dump($prospect);

            if( $prospect === NULL ) {
                // Creer le nouveau data prospect avec les données tag du formulaire
                $prospect = new Prospect();
                $prospect->setNom( $tag1->getName() );
                $prospect->setMail( $tag2->getName() ); 
                $prospect->setAddresseIP($_SERVER['REMOTE_ADDR']);
                $prospectRepository->add($prospect, true);
            } 
            
            // Creer la data prospect et message
            $message = new Message();
            $message->setContenu( $tag3->getName() );
            $message->setIDProspect($prospect );
            $message->setDateEnvoi(new \DateTime());
           

            // Add la data dans la table list
            $messageRepository->add($message);
            

            // ... do your form processing, like saving the Task and Tag entities
        }

        return $this->renderForm('task/new.html.twig', [
            'form' => $form,
        ]);
    }
}