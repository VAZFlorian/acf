<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* espace_admin/index.html.twig */
class __TwigTemplate_1fde5cf9cb8e0555cdb0ea6ba3ef4d47 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "espace_admin/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "espace_admin/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "espace_admin/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "ACF: Admin";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 10
        echo "
    ";
        // line 11
        $this->loadTemplate("header_admin.html.twig", "espace_admin/index.html.twig", 11)->display($context);
        // line 12
        echo "    
    <style>
        img {
            max-width : 100px;
            max-height: 100px;
        }
    </style>

    <main class=\"bg-pierre\">

        <h1 id=\"H1-Admin\" class=\"text-center bg-blanc\">Espace Admin</h1>
         
        <div class=\"row m-2 p-2 bg-blanc\"> 
            <?php 
            \$tableau = [1,2,3,4,5, 6, 7, 8, 9];
            foreach (\$tableau as &\$menuAdmin) {
                ?>
                
                <div class=\"col border border-danger p-3 m-5\">

                    <img class=\"fit-picture\"
                        src=\"https://cdn.discordapp.com/attachments/905880440768966718/953275221832523846/U7NQ7x0bdAY8cAAAAAAAAAAAARPMvWqd4ise4zEIAAAAASUVORK5CYII.png\"
                        alt=\"Image Menu Admin\">
                        
                        <a href=\"https://www.mozilla.org/fr/\"> <?php echo(\$menuAdmin . \"coucou\") ?></a>
                        
                        <p> Prospect </p> 
                </div>

                <div class=\"col border border-danger p-3 m-5\">

                    <img class=\"fit-picture\"
                        src=\"https://cdn.discordapp.com/attachments/905880440768966718/953275221832523846/U7NQ7x0bdAY8cAAAAAAAAAAAARPMvWqd4ise4zEIAAAAASUVORK5CYII.png\"
                        alt=\"Image Menu Admin\">
                        
                        <a href=\"https://www.mozilla.org/fr/\"> <?php echo(\$menuAdmin . \"coucou\") ?></a>
                        
                        <p> Message </p> 
                </div>

                <div class=\"col border border-danger p-3 m-5\">

                    <img class=\"fit-picture\"
                        src=\"https://cdn.discordapp.com/attachments/905880440768966718/953275221832523846/U7NQ7x0bdAY8cAAAAAAAAAAAARPMvWqd4ise4zEIAAAAASUVORK5CYII.png\"
                        alt=\"Image Menu Admin\">
                        
                        <a href=\"https://www.mozilla.org/fr/\"> <?php echo(\$menuAdmin . \"coucou\") ?></a>
                        
                        <p> Rendez Vous </p> 
                </div> 

                <div class=\"col border border-danger p-3 m-5\">

                    <img class=\"fit-picture\"
                        src=\"https://cdn.discordapp.com/attachments/905880440768966718/953275221832523846/U7NQ7x0bdAY8cAAAAAAAAAAAARPMvWqd4ise4zEIAAAAASUVORK5CYII.png\"
                        alt=\"Image Menu Admin\">
                        
                        <a href=\"https://www.mozilla.org/fr/\"> <?php echo(\$menuAdmin . \"coucou\") ?></a>
                        
                        <p> Autre Menu Admin </p> 
                </div>

                <?php
            } 
            ?>  
        </div>


    <div class=\"border m-2 p-2 bg-blanc\">
        <h2 id=\"H2-Prospect\">Prospect index</h2>

        <table class=\"table\">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>AddresseIP</th>
                    <th>Mail</th>
                    <th>Telephone</th>
                    <th>FirstConnexion</th>
                    <th>LastConnexion</th>
                    <th>Nom</th>
                    <th>actions</th>
                </tr>
            </thead>
            <tbody>
            ";
        // line 97
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["prospects"]) || array_key_exists("prospects", $context) ? $context["prospects"] : (function () { throw new RuntimeError('Variable "prospects" does not exist.', 97, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["prospect"]) {
            // line 98
            echo "                <tr>
                    <td>";
            // line 99
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["prospect"], "id", [], "any", false, false, false, 99), "html", null, true);
            echo "</td>
                    <td>";
            // line 100
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["prospect"], "addresseIP", [], "any", false, false, false, 100), "html", null, true);
            echo "</td>
                    <td>";
            // line 101
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["prospect"], "mail", [], "any", false, false, false, 101), "html", null, true);
            echo "</td>
                    <td>";
            // line 102
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["prospect"], "telephone", [], "any", false, false, false, 102), "html", null, true);
            echo "</td>
                    <td>";
            // line 103
            ((twig_get_attribute($this->env, $this->source, $context["prospect"], "firstConnexion", [], "any", false, false, false, 103)) ? (print (twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["prospect"], "firstConnexion", [], "any", false, false, false, 103), "Y-m-d H:i:s"), "html", null, true))) : (print ("")));
            echo "</td>
                    <td>";
            // line 104
            ((twig_get_attribute($this->env, $this->source, $context["prospect"], "lastConnexion", [], "any", false, false, false, 104)) ? (print (twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["prospect"], "lastConnexion", [], "any", false, false, false, 104), "Y-m-d H:i:s"), "html", null, true))) : (print ("")));
            echo "</td>
                    <td>";
            // line 105
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["prospect"], "nom", [], "any", false, false, false, 105), "html", null, true);
            echo "</td>
                    <td>
                        <a href=\"";
            // line 107
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_prospect_show", ["id" => twig_get_attribute($this->env, $this->source, $context["prospect"], "id", [], "any", false, false, false, 107)]), "html", null, true);
            echo "\">show</a>
                        <a href=\"";
            // line 108
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_prospect_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["prospect"], "id", [], "any", false, false, false, 108)]), "html", null, true);
            echo "\">edit</a>
                    </td>
                </tr>
            ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 112
            echo "                <tr>
                    <td colspan=\"8\">no records found</td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['prospect'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 116
        echo "            </tbody>
        </table>
    
        <a href=\"";
        // line 119
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_prospect_new");
        echo "\">Create new</a>
        <a href=\"#Top-HeaderAdmin\"> <h3>Top</h3> </a>


    </div>

    <div class=\"border m-2 p-2 bg-blanc\">
        <h2 id=\"H2-Message\">Message index</h2>

        <table class=\"table\">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nom de l'auteur</th>
                  
                    <th>Contenu</th>
                    <th>DateEnvoi</th>
                    <th>actions</th>
                </tr>
            </thead>
            <tbody>
            ";
        // line 140
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new RuntimeError('Variable "messages" does not exist.', 140, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 141
            echo "                <tr>
                    <td>";
            // line 142
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["message"], "id", [], "any", false, false, false, 142), "html", null, true);
            echo "</td>
                    <td><a href=\"http://localhost:8000/prospect/";
            // line 143
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["message"], "idProspect", [], "any", false, false, false, 143), "id", [], "any", false, false, false, 143), "html", null, true);
            echo "\">  ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["message"], "idProspect", [], "any", false, false, false, 143), "nom", [], "any", false, false, false, 143), "html", null, true);
            echo " </a> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["message"], "idProspect", [], "any", false, false, false, 143), "id", [], "any", false, false, false, 143), "html", null, true);
            echo " </td>
                    
                    <td>";
            // line 145
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["message"], "contenu", [], "any", false, false, false, 145), "html", null, true);
            echo "</td>
                
                    <td>";
            // line 147
            ((twig_get_attribute($this->env, $this->source, $context["message"], "dateEnvoi", [], "any", false, false, false, 147)) ? (print (twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["message"], "dateEnvoi", [], "any", false, false, false, 147), "Y-m-d H:i:s"), "html", null, true))) : (print ("")));
            echo "</td>
                    <td>
                        <a href=\"";
            // line 149
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_message_show", ["id" => twig_get_attribute($this->env, $this->source, $context["message"], "id", [], "any", false, false, false, 149)]), "html", null, true);
            echo "\">show</a>
                        <a href=\"";
            // line 150
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_message_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["message"], "id", [], "any", false, false, false, 150)]), "html", null, true);
            echo "\">edit</a>
                    </td>
                </tr>
            ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 154
            echo "                <tr>
                    <td colspan=\"4\">no records found</td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 158
        echo "            </tbody>
        </table>

        <a href=\"";
        // line 161
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_message_new");
        echo "\">Create new</a>
        <a href=\"#Top-HeaderAdmin\"> <h3>Top</h3> </a>

    </div>

    <div class=\"border m-2 p-2 bg-blanc\">
        <h2 id=\"H2-RendezVous\">Rendez Vous</h2>
        <p> A faire </p>
        <a href=\"#Top-HeaderAdmin\"> <h3>Top</h3> </a>
    </div>

    <div class=\"border m-2 p-2 bg-blanc\">
        <h2 id=\"H2-AutresMenu\">Autres menu admin</h2>
        <p> A faire </p>
        <a href=\"#Top-HeaderAdmin\"> <h3>Top</h3> </a>
    </div>

    </main>


  

</body>
</html>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 7
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 8
        echo "        <link rel=\"stylesheet\" href='./css/style.css' />
    ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "espace_admin/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  377 => 8,  367 => 7,  332 => 161,  327 => 158,  318 => 154,  309 => 150,  305 => 149,  300 => 147,  295 => 145,  286 => 143,  282 => 142,  279 => 141,  274 => 140,  250 => 119,  245 => 116,  236 => 112,  227 => 108,  223 => 107,  218 => 105,  214 => 104,  210 => 103,  206 => 102,  202 => 101,  198 => 100,  194 => 99,  191 => 98,  186 => 97,  99 => 12,  97 => 11,  94 => 10,  92 => 7,  89 => 6,  79 => 5,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}ACF: Admin{% endblock %}

{% block body %}

    {% block stylesheets %}
        <link rel=\"stylesheet\" href='./css/style.css' />
    {% endblock %}

    {% include 'header_admin.html.twig'%}
    
    <style>
        img {
            max-width : 100px;
            max-height: 100px;
        }
    </style>

    <main class=\"bg-pierre\">

        <h1 id=\"H1-Admin\" class=\"text-center bg-blanc\">Espace Admin</h1>
         
        <div class=\"row m-2 p-2 bg-blanc\"> 
            <?php 
            \$tableau = [1,2,3,4,5, 6, 7, 8, 9];
            foreach (\$tableau as &\$menuAdmin) {
                ?>
                
                <div class=\"col border border-danger p-3 m-5\">

                    <img class=\"fit-picture\"
                        src=\"https://cdn.discordapp.com/attachments/905880440768966718/953275221832523846/U7NQ7x0bdAY8cAAAAAAAAAAAARPMvWqd4ise4zEIAAAAASUVORK5CYII.png\"
                        alt=\"Image Menu Admin\">
                        
                        <a href=\"https://www.mozilla.org/fr/\"> <?php echo(\$menuAdmin . \"coucou\") ?></a>
                        
                        <p> Prospect </p> 
                </div>

                <div class=\"col border border-danger p-3 m-5\">

                    <img class=\"fit-picture\"
                        src=\"https://cdn.discordapp.com/attachments/905880440768966718/953275221832523846/U7NQ7x0bdAY8cAAAAAAAAAAAARPMvWqd4ise4zEIAAAAASUVORK5CYII.png\"
                        alt=\"Image Menu Admin\">
                        
                        <a href=\"https://www.mozilla.org/fr/\"> <?php echo(\$menuAdmin . \"coucou\") ?></a>
                        
                        <p> Message </p> 
                </div>

                <div class=\"col border border-danger p-3 m-5\">

                    <img class=\"fit-picture\"
                        src=\"https://cdn.discordapp.com/attachments/905880440768966718/953275221832523846/U7NQ7x0bdAY8cAAAAAAAAAAAARPMvWqd4ise4zEIAAAAASUVORK5CYII.png\"
                        alt=\"Image Menu Admin\">
                        
                        <a href=\"https://www.mozilla.org/fr/\"> <?php echo(\$menuAdmin . \"coucou\") ?></a>
                        
                        <p> Rendez Vous </p> 
                </div> 

                <div class=\"col border border-danger p-3 m-5\">

                    <img class=\"fit-picture\"
                        src=\"https://cdn.discordapp.com/attachments/905880440768966718/953275221832523846/U7NQ7x0bdAY8cAAAAAAAAAAAARPMvWqd4ise4zEIAAAAASUVORK5CYII.png\"
                        alt=\"Image Menu Admin\">
                        
                        <a href=\"https://www.mozilla.org/fr/\"> <?php echo(\$menuAdmin . \"coucou\") ?></a>
                        
                        <p> Autre Menu Admin </p> 
                </div>

                <?php
            } 
            ?>  
        </div>


    <div class=\"border m-2 p-2 bg-blanc\">
        <h2 id=\"H2-Prospect\">Prospect index</h2>

        <table class=\"table\">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>AddresseIP</th>
                    <th>Mail</th>
                    <th>Telephone</th>
                    <th>FirstConnexion</th>
                    <th>LastConnexion</th>
                    <th>Nom</th>
                    <th>actions</th>
                </tr>
            </thead>
            <tbody>
            {% for prospect in prospects %}
                <tr>
                    <td>{{ prospect.id }}</td>
                    <td>{{ prospect.addresseIP }}</td>
                    <td>{{ prospect.mail }}</td>
                    <td>{{ prospect.telephone }}</td>
                    <td>{{ prospect.firstConnexion ? prospect.firstConnexion|date('Y-m-d H:i:s') : '' }}</td>
                    <td>{{ prospect.lastConnexion ? prospect.lastConnexion|date('Y-m-d H:i:s') : '' }}</td>
                    <td>{{ prospect.nom }}</td>
                    <td>
                        <a href=\"{{ path('app_prospect_show', {'id': prospect.id}) }}\">show</a>
                        <a href=\"{{ path('app_prospect_edit', {'id': prospect.id}) }}\">edit</a>
                    </td>
                </tr>
            {% else %}
                <tr>
                    <td colspan=\"8\">no records found</td>
                </tr>
            {% endfor %}
            </tbody>
        </table>
    
        <a href=\"{{ path('app_prospect_new') }}\">Create new</a>
        <a href=\"#Top-HeaderAdmin\"> <h3>Top</h3> </a>


    </div>

    <div class=\"border m-2 p-2 bg-blanc\">
        <h2 id=\"H2-Message\">Message index</h2>

        <table class=\"table\">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nom de l'auteur</th>
                  
                    <th>Contenu</th>
                    <th>DateEnvoi</th>
                    <th>actions</th>
                </tr>
            </thead>
            <tbody>
            {% for message in messages %}
                <tr>
                    <td>{{ message.id }}</td>
                    <td><a href=\"http://localhost:8000/prospect/{{message.idProspect.id}}\">  {{message.idProspect.nom}} </a> {{message.idProspect.id}} </td>
                    
                    <td>{{ message.contenu }}</td>
                
                    <td>{{ message.dateEnvoi ? message.dateEnvoi|date('Y-m-d H:i:s') : '' }}</td>
                    <td>
                        <a href=\"{{ path('app_message_show', {'id': message.id}) }}\">show</a>
                        <a href=\"{{ path('app_message_edit', {'id': message.id}) }}\">edit</a>
                    </td>
                </tr>
            {% else %}
                <tr>
                    <td colspan=\"4\">no records found</td>
                </tr>
            {% endfor %}
            </tbody>
        </table>

        <a href=\"{{ path('app_message_new') }}\">Create new</a>
        <a href=\"#Top-HeaderAdmin\"> <h3>Top</h3> </a>

    </div>

    <div class=\"border m-2 p-2 bg-blanc\">
        <h2 id=\"H2-RendezVous\">Rendez Vous</h2>
        <p> A faire </p>
        <a href=\"#Top-HeaderAdmin\"> <h3>Top</h3> </a>
    </div>

    <div class=\"border m-2 p-2 bg-blanc\">
        <h2 id=\"H2-AutresMenu\">Autres menu admin</h2>
        <p> A faire </p>
        <a href=\"#Top-HeaderAdmin\"> <h3>Top</h3> </a>
    </div>

    </main>


  

</body>
</html>
{% endblock %}
", "espace_admin/index.html.twig", "/var/www/html/ACF/templates/espace_admin/index.html.twig");
    }
}
