<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* header.html.twig */
class __TwigTemplate_a24e74be80c50a83fc4eded2a0e3d1b2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "header.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "header.html.twig"));

        // line 1
        echo "                   
    <header class = \"\">
  
    

        <nav>   
         <img class=\"logo\" src=\"./images/logo.jpg\" alt=\"logo\" />
            <ul class=\"nav justify-content-end navcolor\" >
                <a class=\"nav-link active color\" href='";
        // line 9
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("app_index");
        echo "'><li >Accueil</li></a>
                <a  class=\"nav-link active color\"href=\"\"><li class='nav-item'>Prendre RDV</li></a>
                <a class=\"nav-link active color\" href=\"\"><li class='nav-item'>Devis</li></a>
                <a class=\"nav-link active color\" href=\"\"><li  class='nav-item'>Notre Entreprise</li></a>
                <a class=\"nav-link active color\" href='";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("app_contact");
        echo "'><li class='nav-item'>Nous contacter</li></a>
                <a class=\"nav-link active color\" href='";
        // line 14
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("app_login");
        echo "'><li class='nav-item'>Se connecter</li></a>
            </ul>
         
        </nav> 
   
    </header>
        ";
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 14,  60 => 13,  53 => 9,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("                   
    <header class = \"\">
  
    

        <nav>   
         <img class=\"logo\" src=\"./images/logo.jpg\" alt=\"logo\" />
            <ul class=\"nav justify-content-end navcolor\" >
                <a class=\"nav-link active color\" href='{{ url('app_index') }}'><li >Accueil</li></a>
                <a  class=\"nav-link active color\"href=\"\"><li class='nav-item'>Prendre RDV</li></a>
                <a class=\"nav-link active color\" href=\"\"><li class='nav-item'>Devis</li></a>
                <a class=\"nav-link active color\" href=\"\"><li  class='nav-item'>Notre Entreprise</li></a>
                <a class=\"nav-link active color\" href='{{ url('app_contact') }}'><li class='nav-item'>Nous contacter</li></a>
                <a class=\"nav-link active color\" href='{{ url('app_login') }}'><li class='nav-item'>Se connecter</li></a>
            </ul>
         
        </nav> 
   
    </header>
        ", "header.html.twig", "/var/www/html/ACF/templates/header.html.twig");
    }
}
