<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* header_admin.html.twig */
class __TwigTemplate_b9448a0ba451da168e519f61cb874bb6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "header_admin.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "header_admin.html.twig"));

        // line 1
        echo "                   
<header id=\"Top-HeaderAdmin\" class = \"bg-pierre\">
    <nav>   
        <img class=\"logo\" src=\"./images/logo.jpg\" alt=\"logo\" />
        <ul class=\"nav justify-content-end navcolor\" >
            <a class=\"nav-link active color\" href=\"#H2-Prospect\"><li >Prospect</li></a>
            <a class=\"nav-link active color\" href=\"#H2-Message\"><li class='nav-item'>Message</li></a>
            <a class=\"nav-link active color\" href=\"#H2-RendezVous\"><li class='nav-item'>Rendez Vous</li></a>
            <a class=\"nav-link active color\" href=\"#H2-AutresMenu\"><li class='nav-item'>Autre ...</li></a>
            <a class=\"nav-link active color\" href='";
        // line 10
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("app_logout");
        echo "'><li class='nav-item'>Deconnexion</li></a>
        </ul>         
    </nav> 
</header>
        ";
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "header_admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 10,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("                   
<header id=\"Top-HeaderAdmin\" class = \"bg-pierre\">
    <nav>   
        <img class=\"logo\" src=\"./images/logo.jpg\" alt=\"logo\" />
        <ul class=\"nav justify-content-end navcolor\" >
            <a class=\"nav-link active color\" href=\"#H2-Prospect\"><li >Prospect</li></a>
            <a class=\"nav-link active color\" href=\"#H2-Message\"><li class='nav-item'>Message</li></a>
            <a class=\"nav-link active color\" href=\"#H2-RendezVous\"><li class='nav-item'>Rendez Vous</li></a>
            <a class=\"nav-link active color\" href=\"#H2-AutresMenu\"><li class='nav-item'>Autre ...</li></a>
            <a class=\"nav-link active color\" href='{{ url('app_logout') }}'><li class='nav-item'>Deconnexion</li></a>
        </ul>         
    </nav> 
</header>
        ", "header_admin.html.twig", "/var/www/html/ACF/templates/header_admin.html.twig");
    }
}
