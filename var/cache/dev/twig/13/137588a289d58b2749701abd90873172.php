<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index/index.html.twig */
class __TwigTemplate_65edf6b7f5b542a48cafbaa952b2bf9d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "index/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Hello IndexController!";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "
";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 11
        echo "


<div>
 ";
        // line 15
        $this->loadTemplate("header.html.twig", "index/index.html.twig", 15)->display($context);
        // line 16
        echo " <main class=\"flexligne main\">

        <div class=\"gauche gradient m-3 p-3 col-5\">

            <h2 class=\"nom\">AUDE COURTAGE ET FINANCE (A.C.F.)</h2>
            

            <div class=\"presentation\">

                <img class=\"directeur m-3 border border-dark border-3\" src=\"./images/directeur.jpg\" alt=\"\" />
<div class='hauteurtw'>
                <h1 class=\"tw\">
 <p class=\"typewrite\" data-period=\"2000\" data-type='[ \"Bonjour\", \"Je suis Olivier Galli\", \"Courtier \" ]'>
    <span class=\"wrap\"></span>
  </p>
</h1>
</div>


            </div>
            <div class=\"centrer\">
                <p>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa,
                    tenetur. Perspiciatis culpa dignissimos magnam delectus quam
                    voluptatem! Explicabo facere numquam libero itaque non.
                </p>
                <h2>Une expertise dans de nombreux domaines</h2>
                <div class=\"domaines\">
                   
                    <div class=\"boutons\">
                        <button class=\"btn btn-danger\">

                            ";
        // line 51
        echo "                           Retraite
                        </button>


                        <button class=\"btn btn-danger\" class=\"img-fluid\">
   ";
        // line 58
        echo "                            Santé</button>

                            <button class=\"btn btn-danger\">
                                ";
        // line 62
        echo "       ";
        // line 64
        echo "                                Mobilité</button>
                    </div>
                    </div>
                   <button class=\"devisbtn\" >Réalisez un devis en ligne</button>
                
            </div>
        </div>

        <div class=\"droite  gradient m-3 col\">
            <div class=\"haute\">
              ";
        // line 77
        echo "                
                    <h2>Nous proposons plusieurs services</h2>
<div class=\"boutons\">
                    <button class=\"btn btn-danger\" >
                        ";
        // line 84
        echo "                        Conseil</button>

                        <button class=\"btn btn-danger\">
                         ";
        // line 90
        echo "                            Blabla</button>

                            <button class=\"btn btn-danger\">
                        ";
        // line 96
        echo "                                blabla</button>
                </div>
            </div>
            <div class=\"basse\">
                <div class=\"carte\">
                    <iframe
                        src=\"https://www.google.com/maps/embed?pb=!1m24!1m8!1m3!1d2909.294170636795!2d2.9767065154825367!3d43.18233802914027!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x12b1ac52df463bb5%3A0x772c6d64550a4c45!2s30!3m2!1d43.181828499999995!2d2.9785714!4m5!1s0x12b1ac52eda783b9%3A0x6be966a92153c577!2sAvenue%20du%20Dr%20Paul%20Pompidor%2C%2011100%20Narbonne!3m2!1d43.182847599999995!2d2.979219!5e0!3m2!1sfr!2sfr!4v1647342146855!5m2!1sfr!2sfr\"
                        width=\"400\" height=\"300\"  allowfullscreen=\"\" loading=\"lazy\"></iframe>
                </div>
                      <h3>Nous appeler</h3>
                <div class=\"contacter\">

                    <i class=\"fa-solid fa-phone\"></i>
              
                    
                    <button class=\"btn btn-danger\">Je souhaite être rappelé</button>
                    <button class=\"btn btn-danger\">Prendre un rendez-vous</button>
                </div>
            </div>
    </main>

    <div class = \"flexligne gradientwall\">
        <div class = \"gauche gradient col-5 m-5\">

            <h2 class = \"text-center p-2\"> Avis Client </h2>
            <div id=\"carouselExampleIndicators\" class=\"carousel slide carousel-dark\" data-bs-ride=\"carousel\">
                <div class=\"carousel-indicators \">
                  <button type=\"button\" data-bs-target=\"#carouselExampleIndicators\" data-bs-slide-to=\"0\" class=\"active btn \" aria-current=\"true\" aria-label=\"Slide 1\"></button>
                  <button type=\"button\" data-bs-target=\"#carouselExampleIndicators\" data-bs-slide-to=\"1\" class=\"btn\" aria-label=\"Slide 2\"></button>
                  <button type=\"button\" data-bs-target=\"#carouselExampleIndicators\" data-bs-slide-to=\"2\" class=\"btn\" aria-label=\"Slide 3\"></button>
                </div>
                <div class=\"carousel-inner\">
                  <div class=\"carousel-item active\">
                    
                    <div class=\"m-5 p-5 avisuser\">
                        <img src=\"https://static.wixstatic.com/media/4a93132f85914ba1bab7aac4a8ec2423.jpg/v1/crop/x_0,y_137,w_806,h_377/fill/w_656,h_307,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/man%207.jpg\"
                            style=\"height:25vh \" class=\"p-3\" alt=\"\">
                        <h3></h3>
                        <h3></h3>
                        <p>avant à découvert constamment depuis que j'ai croisé Olivier, j'ai énormément appris...dorénavant mes mois sont tous positifs et j ai même une énorme trésorerie d'avance....incroyable !!! comment n'avais je pu voir cette possibilité, cela parait tellement évident maintenant
                            merci Olivier.</p>

                        <p>L.V.</p>
                    </div>
    
                    
                  </div>
                  <div class=\"carousel-item\">
                    
                    <div class=\"m-5 p-5 avisuser\">
                        <img src=\"https://static.wixstatic.com/media/6848fde432ce40e6a7ca6f5a8191fdce.jpg/v1/crop/x_0,y_148,w_820,h_383/fill/w_656,h_307,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/woman%208.jpg\"
                        style=\"height:25vh \" class=\"p-3\" alt=\"\">
                        <h3></h3>
                        <h3></h3>
                        <p>Merci pour le service rendu....nous avons passé de longues heures ensembles mais je sais dorénavant ou je vais grâce à toi, Olivier. A très vite</p>
                        <p>F.A.</p>
                    </div>
                    
                  </div>
                  <div class=\"carousel-item\">
    
                    <div class=\"m-5 p-5 avisuser\">
                        <img src=\"https://static.wixstatic.com/media/43c856946bae4791986cd49c5bec28b2.jpg/v1/crop/x_0,y_142,w_825,h_386/fill/w_656,h_307,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/man%208.jpg\"
                        style=\"height:25vh \" class=\"p-3\" alt=\"\">
                        <h3></h3>
                        <h3></h3>
                        <p>« depuis des années je cumulais de pauvres contrats qui enrichissaient les autres mais pas moi....merci Olivier...dorénavant mes nouveaux contrats m'enrichissent d'abord à moi.
                            ;) »</p>
                            <p>O.J.</p>
                    </div>
    
                  </div>
                </div>
                <button class=\"carousel-control-prev\" type=\"button\" data-bs-target=\"#carouselExampleIndicators\" data-bs-slide=\"prev\">
                  <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
                  <span class=\"visually-hidden\">Previous</span>
                </button>
                <button class=\"carousel-control-next\" type=\"button\" data-bs-target=\"#carouselExampleIndicators\" data-bs-slide=\"next\">
                  <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
                  <span class=\"visually-hidden\">Next</span>
                </button>
              </div>
        </div>

        <div class = \"droite  gradient col m-3 p-3\">
            <p>  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris nibh erat, finibus quis aliquam nec, tincidunt non justo. Sed ac dui lacus. Phasellus vel dui ac justo rhoncus consectetur. Proin fermentum fermentum quam, facilisis tempus odio venenatis at. Praesent dignissim ipsum sit amet elit fringilla, sit amet varius diam tincidunt. Nunc eu pharetra mauris, ut hendrerit massa. Nam blandit sem nibh, vehicula volutpat nisl convallis ut. Cras venenatis arcu at ante sodales gravida et eget mauris. Quisque hendrerit malesuada lectus, at viverra nibh feugiat at. Proin non euismod erat. Proin viverra, risus ut lacinia dapibus, urna velit sagittis diam, eu dictum justo libero nec risus. Etiam placerat ligula et nibh vulputate, vel vestibulum augue tincidunt. Sed libero orci, rhoncus eu malesuada eget, eleifend eget nisl.

                Vestibulum mattis luctus commodo. Morbi dignissim risus eu facilisis mollis. Aliquam mollis tellus et tincidunt fermentum. Proin dapibus molestie turpis, sit amet imperdiet nisi egestas sit amet. Curabitur vehicula erat metus. Maecenas tristique ultrices felis, non tincidunt massa eleifend vel. Vestibulum porta mattis arcu ac pulvinar. Donec vestibulum quis justo a porttitor. Nulla bibendum neque a velit elementum mollis at ac tortor. </p>
        </div>
        

    </div>
    <footer class=\"col-5\">

        <link rel=\"stylesheet\" href=\"https://unpkg.com/flickity@2/dist/flickity.min.css\">

        <style>



            .carousel {
                background: #EEE;
              }
              
              .carousel-cell {
                width: 20%;
                height: auto;
                margin-right: 10px;
                background: #8C8;
                border-radius: 5px;
                counter-increment: gallery-cell;
              }
              
              /* cell number */
              .carousel-cell:before {
                display: block;
                text-align: center;
                content: counter(gallery-cell);
                line-height: 200px;
                font-size: 80px;
                color: white;
              }
            </style>
        
        
        <div class=\"carousel\" data-flickity>
            <div class=\"carousel-cell\"></div>
            <div class=\"carousel-cell\"></div>
            <div class=\"carousel-cell\"></div>
            <div class=\"carousel-cell\"></div>
            <div class=\"carousel-cell\"></div>
          </div>
    </footer>
    <script src=\"https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js\"></script>

    <script src=\"./js/app.js\"></script>
</body>

</html>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 9
        echo "<link rel=\"stylesheet\" href='./css/style.css' />
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "index/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  337 => 9,  327 => 8,  178 => 96,  173 => 90,  168 => 84,  162 => 77,  150 => 64,  148 => 62,  143 => 58,  136 => 51,  102 => 16,  100 => 15,  94 => 11,  92 => 8,  89 => 7,  79 => 6,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Hello IndexController!{% endblock %}


{% block body %}

{% block stylesheets %}
<link rel=\"stylesheet\" href='./css/style.css' />
{% endblock %}



<div>
 {% include 'header.html.twig'%}
 <main class=\"flexligne main\">

        <div class=\"gauche gradient m-3 p-3 col-5\">

            <h2 class=\"nom\">AUDE COURTAGE ET FINANCE (A.C.F.)</h2>
            

            <div class=\"presentation\">

                <img class=\"directeur m-3 border border-dark border-3\" src=\"./images/directeur.jpg\" alt=\"\" />
<div class='hauteurtw'>
                <h1 class=\"tw\">
 <p class=\"typewrite\" data-period=\"2000\" data-type='[ \"Bonjour\", \"Je suis Olivier Galli\", \"Courtier \" ]'>
    <span class=\"wrap\"></span>
  </p>
</h1>
</div>


            </div>
            <div class=\"centrer\">
                <p>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa,
                    tenetur. Perspiciatis culpa dignissimos magnam delectus quam
                    voluptatem! Explicabo facere numquam libero itaque non.
                </p>
                <h2>Une expertise dans de nombreux domaines</h2>
                <div class=\"domaines\">
                   
                    <div class=\"boutons\">
                        <button class=\"btn btn-danger\">

                            {# <img class=\"img-fluid\"
                            src=\"https://www.mon-guide-retraite.fr/wp-content/uploads/2012/12/Assurance-retraite-540x3591.jpg\"
                            alt=\"Image\"> #}
                           Retraite
                        </button>


                        <button class=\"btn btn-danger\" class=\"img-fluid\">
   {#   src=\"https://www.mon-guide-retraite.fr/wp-content/uploads/2012/12/Assurance-retraite-540x3591.jpg\"
     alt=\"Grapefruit slice atop a pile of other slices\"> #}
                            Santé</button>

                            <button class=\"btn btn-danger\">
                                {# <img class=\"img-fluid\" #}
       {#   src=\"https://www.mon-guide-retraite.fr/wp-content/uploads/2012/12/Assurance-retraite-540x3591.jpg\"
         alt=\"Grapefruit slice atop a pile of other slices\"> #}
                                Mobilité</button>
                    </div>
                    </div>
                   <button class=\"devisbtn\" >Réalisez un devis en ligne</button>
                
            </div>
        </div>

        <div class=\"droite  gradient m-3 col\">
            <div class=\"haute\">
              {#   <div class=\"paragraphe\">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                </div> #}
                
                    <h2>Nous proposons plusieurs services</h2>
<div class=\"boutons\">
                    <button class=\"btn btn-danger\" >
                        {# <img class=\"img-fluid\"
 src=\"https://www.mon-guide-retraite.fr/wp-content/uploads/2012/12/Assurance-retraite-540x3591.jpg\"
 alt=\"Grapefruit slice atop a pile of other slices\"> #}
                        Conseil</button>

                        <button class=\"btn btn-danger\">
                         {#    <img class=\"img-fluid\"
     src=\"https://www.mon-guide-retraite.fr/wp-content/uploads/2012/12/Assurance-retraite-540x3591.jpg\"
     alt=\"Grapefruit slice atop a pile of other slices\"> #}
                            Blabla</button>

                            <button class=\"btn btn-danger\">
                        {#         <img class=\"img-fluid\"
         src=\"https://www.mon-guide-retraite.fr/wp-content/uploads/2012/12/Assurance-retraite-540x3591.jpg\"
         alt=\"Grapefruit slice atop a pile of other slices\"> #}
                                blabla</button>
                </div>
            </div>
            <div class=\"basse\">
                <div class=\"carte\">
                    <iframe
                        src=\"https://www.google.com/maps/embed?pb=!1m24!1m8!1m3!1d2909.294170636795!2d2.9767065154825367!3d43.18233802914027!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x12b1ac52df463bb5%3A0x772c6d64550a4c45!2s30!3m2!1d43.181828499999995!2d2.9785714!4m5!1s0x12b1ac52eda783b9%3A0x6be966a92153c577!2sAvenue%20du%20Dr%20Paul%20Pompidor%2C%2011100%20Narbonne!3m2!1d43.182847599999995!2d2.979219!5e0!3m2!1sfr!2sfr!4v1647342146855!5m2!1sfr!2sfr\"
                        width=\"400\" height=\"300\"  allowfullscreen=\"\" loading=\"lazy\"></iframe>
                </div>
                      <h3>Nous appeler</h3>
                <div class=\"contacter\">

                    <i class=\"fa-solid fa-phone\"></i>
              
                    
                    <button class=\"btn btn-danger\">Je souhaite être rappelé</button>
                    <button class=\"btn btn-danger\">Prendre un rendez-vous</button>
                </div>
            </div>
    </main>

    <div class = \"flexligne gradientwall\">
        <div class = \"gauche gradient col-5 m-5\">

            <h2 class = \"text-center p-2\"> Avis Client </h2>
            <div id=\"carouselExampleIndicators\" class=\"carousel slide carousel-dark\" data-bs-ride=\"carousel\">
                <div class=\"carousel-indicators \">
                  <button type=\"button\" data-bs-target=\"#carouselExampleIndicators\" data-bs-slide-to=\"0\" class=\"active btn \" aria-current=\"true\" aria-label=\"Slide 1\"></button>
                  <button type=\"button\" data-bs-target=\"#carouselExampleIndicators\" data-bs-slide-to=\"1\" class=\"btn\" aria-label=\"Slide 2\"></button>
                  <button type=\"button\" data-bs-target=\"#carouselExampleIndicators\" data-bs-slide-to=\"2\" class=\"btn\" aria-label=\"Slide 3\"></button>
                </div>
                <div class=\"carousel-inner\">
                  <div class=\"carousel-item active\">
                    
                    <div class=\"m-5 p-5 avisuser\">
                        <img src=\"https://static.wixstatic.com/media/4a93132f85914ba1bab7aac4a8ec2423.jpg/v1/crop/x_0,y_137,w_806,h_377/fill/w_656,h_307,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/man%207.jpg\"
                            style=\"height:25vh \" class=\"p-3\" alt=\"\">
                        <h3></h3>
                        <h3></h3>
                        <p>avant à découvert constamment depuis que j'ai croisé Olivier, j'ai énormément appris...dorénavant mes mois sont tous positifs et j ai même une énorme trésorerie d'avance....incroyable !!! comment n'avais je pu voir cette possibilité, cela parait tellement évident maintenant
                            merci Olivier.</p>

                        <p>L.V.</p>
                    </div>
    
                    
                  </div>
                  <div class=\"carousel-item\">
                    
                    <div class=\"m-5 p-5 avisuser\">
                        <img src=\"https://static.wixstatic.com/media/6848fde432ce40e6a7ca6f5a8191fdce.jpg/v1/crop/x_0,y_148,w_820,h_383/fill/w_656,h_307,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/woman%208.jpg\"
                        style=\"height:25vh \" class=\"p-3\" alt=\"\">
                        <h3></h3>
                        <h3></h3>
                        <p>Merci pour le service rendu....nous avons passé de longues heures ensembles mais je sais dorénavant ou je vais grâce à toi, Olivier. A très vite</p>
                        <p>F.A.</p>
                    </div>
                    
                  </div>
                  <div class=\"carousel-item\">
    
                    <div class=\"m-5 p-5 avisuser\">
                        <img src=\"https://static.wixstatic.com/media/43c856946bae4791986cd49c5bec28b2.jpg/v1/crop/x_0,y_142,w_825,h_386/fill/w_656,h_307,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/man%208.jpg\"
                        style=\"height:25vh \" class=\"p-3\" alt=\"\">
                        <h3></h3>
                        <h3></h3>
                        <p>« depuis des années je cumulais de pauvres contrats qui enrichissaient les autres mais pas moi....merci Olivier...dorénavant mes nouveaux contrats m'enrichissent d'abord à moi.
                            ;) »</p>
                            <p>O.J.</p>
                    </div>
    
                  </div>
                </div>
                <button class=\"carousel-control-prev\" type=\"button\" data-bs-target=\"#carouselExampleIndicators\" data-bs-slide=\"prev\">
                  <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
                  <span class=\"visually-hidden\">Previous</span>
                </button>
                <button class=\"carousel-control-next\" type=\"button\" data-bs-target=\"#carouselExampleIndicators\" data-bs-slide=\"next\">
                  <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
                  <span class=\"visually-hidden\">Next</span>
                </button>
              </div>
        </div>

        <div class = \"droite  gradient col m-3 p-3\">
            <p>  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris nibh erat, finibus quis aliquam nec, tincidunt non justo. Sed ac dui lacus. Phasellus vel dui ac justo rhoncus consectetur. Proin fermentum fermentum quam, facilisis tempus odio venenatis at. Praesent dignissim ipsum sit amet elit fringilla, sit amet varius diam tincidunt. Nunc eu pharetra mauris, ut hendrerit massa. Nam blandit sem nibh, vehicula volutpat nisl convallis ut. Cras venenatis arcu at ante sodales gravida et eget mauris. Quisque hendrerit malesuada lectus, at viverra nibh feugiat at. Proin non euismod erat. Proin viverra, risus ut lacinia dapibus, urna velit sagittis diam, eu dictum justo libero nec risus. Etiam placerat ligula et nibh vulputate, vel vestibulum augue tincidunt. Sed libero orci, rhoncus eu malesuada eget, eleifend eget nisl.

                Vestibulum mattis luctus commodo. Morbi dignissim risus eu facilisis mollis. Aliquam mollis tellus et tincidunt fermentum. Proin dapibus molestie turpis, sit amet imperdiet nisi egestas sit amet. Curabitur vehicula erat metus. Maecenas tristique ultrices felis, non tincidunt massa eleifend vel. Vestibulum porta mattis arcu ac pulvinar. Donec vestibulum quis justo a porttitor. Nulla bibendum neque a velit elementum mollis at ac tortor. </p>
        </div>
        

    </div>
    <footer class=\"col-5\">

        <link rel=\"stylesheet\" href=\"https://unpkg.com/flickity@2/dist/flickity.min.css\">

        <style>



            .carousel {
                background: #EEE;
              }
              
              .carousel-cell {
                width: 20%;
                height: auto;
                margin-right: 10px;
                background: #8C8;
                border-radius: 5px;
                counter-increment: gallery-cell;
              }
              
              /* cell number */
              .carousel-cell:before {
                display: block;
                text-align: center;
                content: counter(gallery-cell);
                line-height: 200px;
                font-size: 80px;
                color: white;
              }
            </style>
        
        
        <div class=\"carousel\" data-flickity>
            <div class=\"carousel-cell\"></div>
            <div class=\"carousel-cell\"></div>
            <div class=\"carousel-cell\"></div>
            <div class=\"carousel-cell\"></div>
            <div class=\"carousel-cell\"></div>
          </div>
    </footer>
    <script src=\"https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js\"></script>

    <script src=\"./js/app.js\"></script>
</body>

</html>
{% endblock %}
", "index/index.html.twig", "/var/www/html/ACF/templates/index/index.html.twig");
    }
}
