<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contact/index.html.twig */
class __TwigTemplate_5f70d1e9565f9e5b693a035389b19a72 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contact/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contact/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "contact/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "ACF: Contact";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "

    ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 11
        echo "
    ";
        // line 12
        $this->loadTemplate("header.html.twig", "contact/index.html.twig", 12)->display($context);
        // line 13
        echo "
    <main>

        <h1 class=\"text-center m-1 p-1 bgbleu\">Contact & plan d'accès</h1>

        <div class = \"text-center row p-1 m-1\" >
            
            <div class=\"col m-1 p-1 bgbleu bradius-2\">
                <p>Bat. INESS - 2eme étage Bureau n°13 </p>
                <p>30 rue POMPIDOR - 11 100 Narbonne France </p>
                <p>audecourtageetfinance@gmail.com</p>
                <p>+33.06.98.35.90.00</p>
            </div>

            <div class=\"col m-1 p-1 bgbleu bradius-2\">
                <p>Ouvert du lundi au vendredi de 9h à midi et de 14 à 19h</p>
                <p>Fermé le samedi et le dimanche</p>
            </div>
        </div>

        <div class = \"text-center row p-1 m-1\">
            <div class=\"col m-1 p-1 bgbleu bradius-2\">
                <div>  ";
        // line 35
        echo twig_include($this->env, $context, "./task/new.html.twig");
        echo " </div>
            </div>

            <div class=\"col m-1 p-2 bgbleu bradius-2\">
                <iframe
                        src=\"https://www.google.com/maps/embed?pb=!1m24!1m8!1m3!1d2909.294170636795!2d2.9767065154825367!3d43.18233802914027!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x12b1ac52df463bb5%3A0x772c6d64550a4c45!2s30!3m2!1d43.181828499999995!2d2.9785714!4m5!1s0x12b1ac52eda783b9%3A0x6be966a92153c577!2sAvenue%20du%20Dr%20Paul%20Pompidor%2C%2011100%20Narbonne!3m2!1d43.182847599999995!2d2.979219!5e0!3m2!1sfr!2sfr!4v1647342146855!5m2!1sfr!2sfr\"
                        width=\"400\" height=\"300\" style=\"border: 0\" allowfullscreen=\"\" loading=\"lazy\">
                </iframe>
            </div>
        </div>
        
    </main>


    <!-- JavaScript Bundle with Popper -->
    <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p\" crossorigin=\"anonymous\"></script>


";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 9
        echo "        <link rel=\"stylesheet\" href='./css/style.css' />
    ";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "contact/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 9,  153 => 8,  124 => 35,  100 => 13,  98 => 12,  95 => 11,  93 => 8,  89 => 6,  79 => 5,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}ACF: Contact{% endblock %}

{% block body %}


    {% block stylesheets %}
        <link rel=\"stylesheet\" href='./css/style.css' />
    {% endblock %}

    {% include 'header.html.twig'%}

    <main>

        <h1 class=\"text-center m-1 p-1 bgbleu\">Contact & plan d'accès</h1>

        <div class = \"text-center row p-1 m-1\" >
            
            <div class=\"col m-1 p-1 bgbleu bradius-2\">
                <p>Bat. INESS - 2eme étage Bureau n°13 </p>
                <p>30 rue POMPIDOR - 11 100 Narbonne France </p>
                <p>audecourtageetfinance@gmail.com</p>
                <p>+33.06.98.35.90.00</p>
            </div>

            <div class=\"col m-1 p-1 bgbleu bradius-2\">
                <p>Ouvert du lundi au vendredi de 9h à midi et de 14 à 19h</p>
                <p>Fermé le samedi et le dimanche</p>
            </div>
        </div>

        <div class = \"text-center row p-1 m-1\">
            <div class=\"col m-1 p-1 bgbleu bradius-2\">
                <div>  {{ include('./task/new.html.twig') }} </div>
            </div>

            <div class=\"col m-1 p-2 bgbleu bradius-2\">
                <iframe
                        src=\"https://www.google.com/maps/embed?pb=!1m24!1m8!1m3!1d2909.294170636795!2d2.9767065154825367!3d43.18233802914027!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x12b1ac52df463bb5%3A0x772c6d64550a4c45!2s30!3m2!1d43.181828499999995!2d2.9785714!4m5!1s0x12b1ac52eda783b9%3A0x6be966a92153c577!2sAvenue%20du%20Dr%20Paul%20Pompidor%2C%2011100%20Narbonne!3m2!1d43.182847599999995!2d2.979219!5e0!3m2!1sfr!2sfr!4v1647342146855!5m2!1sfr!2sfr\"
                        width=\"400\" height=\"300\" style=\"border: 0\" allowfullscreen=\"\" loading=\"lazy\">
                </iframe>
            </div>
        </div>
        
    </main>


    <!-- JavaScript Bundle with Popper -->
    <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p\" crossorigin=\"anonymous\"></script>


{% endblock %}
", "contact/index.html.twig", "/var/www/html/ACF/templates/contact/index.html.twig");
    }
}
