<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
    <link href="main.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>
    
    <header>
        <div>LE HEADEER</div>
    </header>
    
    <style>
        img {
            max-width : 100px;
            max-height: 100px;
        }

        p {
            background: rgb(183, 236, 183);
        }
    </style>



    <main>

        <h1 class="text-center m-5">Contact & plan d'acces</h1>

        <div class = "text-center row p-5 m-5 border border-danger">
            <div class="col m-5 border">
                

                <p>Bat. INESS - 30 rue POMPIDOR - Bureau n°13 Narbonne France 11100</p>
                <p>audecourtageetfinance@gmail.com</p>
                <p>06.98.35.9000</p>

            </div>

            <div class="col m-5 border">

                <p>Ouvert du lundi au vendredi de 9h à midi et de 14 à 19h</p>
                <p>Fermé le samedi et le dimanche</p>

            </div>
        </div>


        <div class = "text-center row p-5 m-5 border border-danger">
            <div class="col m-5 border">
                
                <div>LE FORMULAIRE</div>

            </div>

            <div class="col m-5 border">

                <div>LA MAP</div>

            </div>
        </div>
        
    </main>


    <!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</body>
</html>